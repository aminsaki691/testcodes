<?php


namespace App\Repository\Blog;


use App\Models\Blog;

class blogRepository implements  InterfaceBlog
{
   public function all()
   {
     return   Blog::with('categroy')->orderBy('id','desc')->get();
   }
   public function create(array $data)
   {
      return  Blog::create($data);
   }
   public function show($id)
   {
       return   Blog::with('categroy')->find($id);
   }
   public function delete($id)
   {
       return   Blog::find($id)->delete();
   }
   public function update(array $data, $id)
   {
      return Blog::find($id)->update($data);
   }

}
