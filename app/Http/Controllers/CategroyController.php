<?php

namespace App\Http\Controllers;

use App\Models\Categroy;
use App\Repository\Categroy\InterfaceCategroy;
use Illuminate\Http\Request;

class CategroyController extends Controller
{
    protected $cate = null;

    public function __construct(InterfaceCategroy  $categroy)
    {
          $this->cate = $categroy;

    }

    public function index()
    {
        $result = Categroy::all();

        return response()->json(['date' => $result], '200');
    }

    public function show($id)
    {
        if (empty($id))
            return response()->json(['date' => "این مقاله وجود ندارد"], '404');


        $result = $this->cate->show($id);

        return response()->json(['date' => $result], '200');

    }

    public function update(Request $request)
    {
        $id = $request->id;
        if (empty($id))
            return response()->json(['date' => "این مقاله وجود ندارد"], '404');


        $resule =  $this->cate->update($request->all(), $id);
        if($resule)
            return response()->json(['data' => "اطلاعات شما با به روز رسانی  شد"], 200);


        return response()->json(['data' => "درج اطلاعات شما با خطا مواجه شد"], 404);


    }

    public function create(Request $request)
    {

        $result = $this->cate->create([
            'name' => $request->name,
             'comments'=> $request->comments

        ]);


        if ($result)
            return response()->json(['data' => "اطلاعات شما با موفقعیت درج شده"], 200);


        return response()->json(['data' => "درج اطلاعات شما با خطا مواجه شده"], 404);

    }

}
