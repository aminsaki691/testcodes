<?php

namespace App\Http\Controllers;

use App\Repository\Blog\InterfaceBlog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    protected $blog = null;

    public function __construct(InterfaceBlog $blog)
    {
        $this->blog = $blog;
    }

    public function index()
    {

        $result = $this->blog->all();

        return response()->json(['date' => $result], '200');
    }

    public function show($id)
    {
        if (empty($id))
            return response()->json(['date' => "این مقاله وجود ندارد"], '404');


        $result = $this->blog->show($id);

        return response()->json(['date' => $result], '200');

    }

    public function update(Request $request)
    {
        $id = $request->id;
        if (empty($id))
            return response()->json(['date' => "این مقاله وجود ندارد"], '404');


         $resule =  $this->blog->update($request->all(), $id);
         if($resule)
             return response()->json(['data' => "اطلاعات شما با به روز رسانی  شد"], 200);


        return response()->json(['data' => "درج اطلاعات شما با خطا مواجه شد"], 404);


    }

    public function create(Request $request)
    {

        $result = $this->blog->create([
            'name' => $request->name,
            'price' => $request->price,
            'count' => $request->count,
            'categroy_id' => $request->categroy_id
        ]);


        if ($result)
            return response()->json(['data' => "اطلاعات شما با موفقعیت درج شده"], 200);


        return response()->json(['data' => "درج اطلاعات شما با خطا مواجه شده"], 404);

    }


}









