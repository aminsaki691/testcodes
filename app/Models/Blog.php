<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    protected $table = "blogs";

    protected  $fillable = [ 'id','name' ,'count' ,'price','categroy_id'];


    public  function categroy(){
        return $this->belongsTo(Blog::class);
    }
}
