<?php

namespace App\Providers;

use App\Repository\Blog\blogRepository;
use App\Repository\Blog\InterfaceBlog;
use App\Repository\Categroy\CategroyRepository;
use App\Repository\Categroy\InterfaceCategroy;
use Illuminate\Support\ServiceProvider;

class EloquentService extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(InterfaceBlog::class , blogRepository::class );
        $this->app->bind(InterfaceCategroy::class ,CategroyRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
