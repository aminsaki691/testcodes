<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\CategroyController;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

##blogs
Route::get('/blog/list', [BlogController::class, 'index']);
Route::get('/blog/show/{id}', [BlogController::class, 'show']);
Route::delete('/blog/delete/{id}', [BlogController::class, 'delete']);
Route::post('/blog/update', [BlogController::class, 'update']);
Route::post('/blog/create', [BlogController::class, 'create']);

##cat
Route::get('/categroy/list', [CategroyController::class, 'index']);
Route::get('/categroy/show/{id}', [CategroyController::class, 'show']);
Route::delete('/categroy/delete/{id}', [CategroyController::class, 'delete']);
Route::post('/categroy/update', [CategroyController::class, 'update']);
Route::post('/categroy/create', [CategroyController::class, 'create']);



